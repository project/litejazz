<?php

/**
 * advf-forum-repeat-post.tpl.php is a blank template file used for the 
 * repeated node on the top of each page of a paginated forum thread.
 * If you leave it empty, subsequent pages will start with the next comment
 * like you typically find in forum software. You could also put a specially
 * formatted teaser to remind people what post they are reading. If you like
 * having the entire node repeated, simply copy the entire contents of 
 * advf-forum-post.tpl.php into this file. All the same variables are available.
 */
?>